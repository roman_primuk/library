package crudapp.users.service;

import crudapp.users.model.entity.User;
import crudapp.users.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @KafkaListener(id = "Starship", topics = {"server.starship"}, containerFactory = "singleFactory")
    public void increment(String userId) {
        User user = userRepository.findById(Integer.valueOf(userId)).get();
        user.setCountOfBook(user.getCountOfBook() + 1);
        userRepository.save(user);
    }
}
